User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

# Create 2 users to be used
User.new(name: "Quiz Master", email: "quizmaster@example.com", password: "password", admin: "true", activated: "true").save
User.new(name: "Brandon Longenecker", email: "b.longenecker7@gmail.com", password: "password", admin: "false", activated: "true").save

# Create a default quiz
Quiz.new(title: "State Capitals", user_id: 101, category: "history").save

Question.new(title: "What is the capital of Arkansas?", quiz_id: 1).save
Answer.new(content: "Fort Smith", correct: false, question_id: 1).save
Answer.new(content: "Hot Springs", correct: false, question_id: 1).save
Answer.new(content: "Little Rock", correct: true, question_id: 1).save
Answer.new(content: "Fayetteville", correct: false, question_id: 1).save

Question.new(title: "What is the capital of Michigan?", quiz_id: 1).save
Answer.new(content: "Lansing", correct: true, question_id: 2).save
Answer.new(content: "Detroit", correct: false, question_id: 2).save
Answer.new(content: "Grand Rapids", correct: false, question_id: 2).save
Answer.new(content: "Ann Arbor", correct: false, question_id: 2).save

Question.new(title: "What is the capital of Nevada?", quiz_id: 1).save
Answer.new(content: "Reno", correct: false, question_id: 3).save
Answer.new(content: "Carson City", correct: true, question_id: 3).save
Answer.new(content: "Henderson", correct: false, question_id: 3).save
Answer.new(content: "Las Vegas", correct: false, question_id: 3).save

Question.new(title: "What is the capital of South Carolina?", quiz_id: 1).save
Answer.new(content: "Charleston", correct: false, question_id: 4).save
Answer.new(content: "Greenville", correct: false, question_id: 4).save
Answer.new(content: "Clemson", correct: false, question_id: 4).save
Answer.new(content: "Columbia", correct: true, question_id: 4).save

Question.new(title: "What is the capital of Vermont?", quiz_id: 1).save
Answer.new(content: "Burlington", correct: false, question_id: 5).save
Answer.new(content: "Montpelier", correct: true, question_id: 5).save
Answer.new(content: "Middlebury", correct: false, question_id: 5).save
Answer.new(content: "Newport", correct: false, question_id: 5).save

# Create another default quiz
Quiz.new(title: "Grammar Quiz", user_id: 101, category: "literature").save

Question.new(title: "I ... tennis every Sunday morning.", quiz_id: 2).save
Answer.new(content: "playing", correct: false, question_id: 6).save
Answer.new(content: "play", correct: true, question_id: 6).save
Answer.new(content: "am playing", correct: false, question_id: 6).save
Answer.new(content: "am play", correct: false, question_id: 6).save

Question.new(title: "Don't make so much noise. Bob ... to study!", quiz_id: 2).save
Answer.new(content: "try", correct: false, question_id: 7).save
Answer.new(content: "tries", correct: false, question_id: 7).save
Answer.new(content: "tried", correct: false, question_id: 7).save
Answer.new(content: "is trying", correct: true, question_id: 7).save

Question.new(title: "Alice ... her teeth every morning.", quiz_id: 2).save
Answer.new(content: "will cleaned", correct: false, question_id: 8).save
Answer.new(content: "is clean", correct: false, question_id: 8).save
Answer.new(content: "cleans", correct: true, question_id: 8).save
Answer.new(content: "clean", correct: false, question_id: 8).save

Question.new(title: "Babies ... when they are hungry.", quiz_id: 2).save
Answer.new(content: "cry", correct: true, question_id: 9).save
Answer.new(content: "cries", correct: false, question_id: 9).save
Answer.new(content: "cried", correct: false, question_id: 9).save
Answer.new(content: "crying", correct: false, question_id: 9).save


# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }

#Fake Quizzes
#users = User.order(:created_at).take(6)
#50.times do
#  content = Faker::Lorem.sentence(5)
#  users.each { |user| user.microposts.create!(content: content) }
#end