class RemoveUsersFromScores < ActiveRecord::Migration[5.0]
  def change
    remove_column :scores, :users, :string
  end
end
