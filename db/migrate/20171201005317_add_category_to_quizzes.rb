class AddCategoryToQuizzes < ActiveRecord::Migration[5.0]
  def change
    add_column :quizzes, :category, :string
  end
end
