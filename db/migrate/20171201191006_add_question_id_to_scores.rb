class AddQuestionIdToScores < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :question_id, :integer
  end
end
