class SupportMailer < ApplicationMailer

  def contact_me(message)
    @message = message
    
    mail to: "dierdorff85@live.com", from: message.email
  end
end
