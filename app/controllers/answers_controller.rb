class AnswersController < ApplicationController
  def index
    @answers = Answer.all
  end
  
  def new
    @answer = Answer.new
    @aNum = Answer.where(question_id: params[:question_id]).count + 1
  end
  
  def show
    @answer = Answer.find(params[:id])
  end
  
  def create
    Question.order (:created_at)
    @question = Question.last
    @answer = @question.answers.build(answer_params)
    if @answer.save
      redirect_to new_answer_url(question_id: @question.id)
    else
      render static_pages/home
    end
  end  
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:content, :correct)
    end
end
