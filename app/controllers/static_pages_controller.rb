class StaticPagesController < ApplicationController
  def home
    #@quiz = current_user.quizzes.build if logged_in?
    if logged_in?
      @quiz = current_user.quizzes.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @feed_items.each do |item|
        @qid = item.id
        @questions = Question.where(quiz_id: @qid)
        @answers = Answer.where(question_id: @questions)
        if(@answers.empty?)
          item.destroy
        end
      end
    end
  end
  
  def help
  end
  
  def about
  end
  
  def contact
  end
  
end