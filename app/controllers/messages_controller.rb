class MessagesController < ApplicationController
  def new
    @message = Message.new
  end

  def create
    message_params = params.require(:message).permit(:name, :email, :content)
    @message = Message.new(message_params)
    
    if @message.valid?
      SupportMailer.contact_me(@message).deliver_now
      redirect_to root_url, notice: "Message received, thanks!"
    else
      render :new
    end
  end

private

end
