class QuizzesController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def index
    if params
      uid = User.where("name LIKE ?", "%#{params[:search]}%").first
      if uid
        @quizzes = Quiz.search(uid.id).order("created_at DESC")
      else
        @quizzes = Quiz.search(params[:search]).order("created_at DESC")
      end
    else
      @quizzes = Quiz.all.order('created_at DESC')
    end
  end
  
  def new
    @quiz = Quiz.new
  end
  
  def show
    @quiz = Quiz.find(params[:id])
  end
  
  def create
    @quiz = current_user.quizzes.build(quiz_params)
    if @quiz.title == nil
      @quiz.title = ""
    end
    if @quiz.title.downcase == 'test' 
      cumulative(@quiz)
    elsif @quiz.save
      redirect_to createQuestion_url(quiz_id: @quiz.id)
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def take
    @q = Quiz.find(params[:quiz_id])

    if !Question.where(quiz_id: @q.id, id: params[:question_id]).empty?
      @question = Question.find(params[:question_id])
      @answers = Answer.where(question_id: @question.id)
      @questionNum = 1 + params[:question_id].to_i - Question.where(quiz_id: params[:quiz_id]).first.id
      @totalQuestions = Question.where(quiz_id: params[:quiz_id]).count.to_i
    end

    @user = current_user

    if params[:answer_id] != nil
      if Score.where(quiz_id: params[:quiz_id], user_id: @user.id, question_id: params[:question_id].to_i - 1).exists?

        @score = Score.find_by(quiz_id: params[:quiz_id], user_id: @user.id, question_id: params[:question_id].to_i - 1)

        if Answer.find(params[:answer_id]).correct?
          @score.value = 1
        else
          @score.value = 0
        end

        @score.save

      else
        if Answer.find(params[:answer_id]).correct?
          Score.new(quiz_id: params[:quiz_id], user_id: @user.id, question_id: params[:question_id].to_i - 1, value: 1).save
        else
          Score.new(quiz_id: params[:quiz_id], user_id: @user.id, question_id: params[:question_id].to_i - 1, value: 0).save
        end
      end
    end

    if Question.where(quiz_id: @q.id, id: params[:question_id]).empty?
      redirect_to results_url(quiz_id: @q.id), method: :post
    end

  end
  
  def destroy
     @quiz.destroy
     flash[:succcess] = "quiz deleted"
     redirect_to  root_url
  end
      
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def quiz_params
      params.require(:quiz).permit(:title, :category)
    end
    
    def correct_user
       @quiz = current_user.quizzes.find_by(id: params[:id])
       redirect_to root_url if @quiz.nil?
    end
    
    def cumulative(quiz)
      quiz = current_user.quizzes.build(quiz_params)#create new quiz in server, not db
      #flash[:notice] = quiz.category
      if quiz.save # save to db
        questions_used = [] #set local tracking array so q's dont repeat
        possible_question_ids = Question.where(quiz_id: Quiz.where(category: quiz.category, user_id: current_user.id)).ids # get array of id's of questions in category for new quiz
        while questions_used.count <= possible_question_ids.count #as many qs in category, does not scale!!!
          current_id = possible_question_ids.sample #pull id from random index
          if !(questions_used.include?(current_id)) #is id already used?
            questions_used.push(current_id) #push on to tracking array
            possible_question_ids.delete(current_id)
            new_question = Question.find(current_id) #get existing question with matching id
            @random_question = quiz.questions.new(title: new_question.title) #create a new identical question for the test
            if @random_question.save #save the new question
              random_answers = Answer.where(question_id: current_id) #add all answers to this new question
              random_answers.each do |ra|
                @new_answer = @random_question.answers.new(content: ra.content, correct: ra.correct)
                if !@new_answer.save
                  flash[:notice] = 'random quiz not created!'
                end
              end
            end
          end
        end
        redirect_to root_url
        flash[:succcess] = 'test created'
      else
        flash[:notice] = 'quiz not saved'
      end
    end
  
end