class QuestionsController < ApplicationController
  def index
    @questions = Question.all
  end
  
  def new
    @question = Question.new
    @qNum = Question.where(quiz_id: params[:quiz_id]).count + 1
  end
  
  def show
    @question = Question.find(params[:id])
  end
  
  def create
    Quiz.order (:created_at)
    @quiz = Quiz.first
    @question = @quiz.questions.build(question_params)
    if @question.save
      redirect_to new_answer_url(question_id: @question.id)
    else
      render 'static_pages/home'
    end
  end  
  
  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title)
    end
end
