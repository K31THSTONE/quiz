class ScoresController < ApplicationController
  def index
    @scores = Score.all
  end

  def new
    @score = Score.new
  end

  def show
    @q = Quiz.find(params[:quiz_id])
    @user = current_user

    if !Score.find_by(quiz_id: params[:quiz_id], user_id: @user.id).nil?

      @correct = Score.where(quiz_id: params[:quiz_id], user_id: @user.id).sum(:value).to_f
    else
      @correct = -1
    end

    @totalQuestions = Question.where(quiz_id: params[:quiz_id]).count.to_f
    @score = @correct/@totalQuestions
    @avgScore = Score.where(quiz_id: params[:quiz_id]).sum(:value).to_f/Score.where(quiz_id: params[:quiz_id]).count.to_f
  end

  def showDetailed
    @q = Quiz.find(params[:quiz_id])

    if !Question.where(quiz_id: @q.id, id: params[:question_id]).empty?
      @question = Question.find(params[:question_id])
      @answers = Answer.where(question_id: @question.id)
      @questionNum = 1 + params[:question_id].to_i - Question.where(quiz_id: params[:quiz_id]).first.id
      @totalQuestions = Question.where(quiz_id: params[:quiz_id]).count.to_i
    end

    @user = current_user

    @correct = Score.find_by(quiz_id: @q.id, question_id: params[:question_id], user_id: @user.id).value
    @totalTaken = Score.where(quiz_id: @q.id, question_id: params[:question_id]).count
    @totalCorrect = Score.where(quiz_id: @q.id, question_id: params[:question_id]).sum(:value)

    if Question.where(quiz_id: @q.id, id: params[:question_id]).empty?
      redirect_to results_url(quiz_id: @q.id), method: :post
    end
  end

  def create
    @scores = Quiz.first.answers.build(answer_params)
    if @score.save
      redirect_to root_url
    else
      render static_pages/home
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:score).permit(:value)
    end
end
