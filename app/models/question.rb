class Question < ApplicationRecord
  belongs_to :quiz
  has_many :answers, dependent: :destroy
  validates :title, presence: :true
  
  def self.get_questions_by_id(quiz_group)
    @questions_by_id = []
    quiz_group.each do |quiz|
      quiz.each do |id|
        new_question = where("quiz_id LIKE ?", id)
        @questions_by_id.push(new_question)
      end
    end
  end
end
