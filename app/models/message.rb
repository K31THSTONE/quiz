class Message
  include ActiveModel::Model
  include ActiveModel::Conversion
  include ActiveModel::Validations

  include ActiveModel::Model
  attr_accessor :name, :email, :content
  validates :name, :email, :content, presence: true

end
