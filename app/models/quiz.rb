class Quiz < ApplicationRecord
  belongs_to :user
  has_many :questions, dependent: :destroy
  has_many :scores, dependent: :destroy
  validates :title, presence: :true
  validates :user_id, presence: :true
  validates :category, presence: :true
  default_scope -> { order(created_at: :desc) }
  
  def self.search(search)
    where("title LIKE ? OR user_id LIKE ? OR category LIKE ?", "%#{search}%", "%#{search}%", "%#{search}%")
  end
  
  def self.quizzes_by_category(category)
    where("category LIKE ?","%#{category}%")
  end
  
end