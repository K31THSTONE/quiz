require 'test_helper'

class QuizzesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @quiz = quizzes(:one)
  end
  
  test "new quiz" do
    get createQuiz_path
    assert_response :success
  end
  
  test "take a quiz" do
  end
  
  test "search bar for quiz" do
  end
  
  test "create quiz" do
    get createQuiz_path 
    assert_response :success 
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Quiz.count' do
      post quizzes_path, params: { quiz: { title: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Quiz.count' do
      delete quiz_path(@quiz)
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for wrong quiz" do
    log_in_as(User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar"))
    quiz = quizzes(:two)
    assert_no_difference 'Quiz.count' do
      delete quiz_path(quiz)
    end
    assert_redirected_to login_url
  end
  
end