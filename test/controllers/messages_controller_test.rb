require 'test_helper'

class MessagesControllerTest < ActionDispatch::IntegrationTest
  test "get new" do
    get new_message_url

    assert_response :success

    assert_select 'form' do 
      assert_select 'input[type=text]'
      assert_select 'input[type=email]'
      assert_select 'textarea'
      assert_select 'input[type=submit]'
    end
  end
  
  test "post create" do
    post create_message_url, params: {
      message: {
       name: 'chris',
       email: 'a@bcd@efg.ghi',
       content: 'testing message post'
      }
    }
    assert_redirected_to root_url

    follow_redirect!

    assert_match /Message received, thanks!/, response.body
  end
  
  
  
end
