require 'test_helper'

class SupportMailerTest < ActionMailer::TestCase
  test "contact_me" do
    # Create the email and store it for further assertions
    email = SupportMailer.contact_me(Message.new(name: 'chris', email: 'a@b.c', content: 'hiya'))
 
    # Send the email, then test that it got queued
    assert_emails 1 do
      email.deliver_now
    end
 
    assert_equal ['dierdorff85@live.com'], email.to
    
  end
end
