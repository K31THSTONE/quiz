require 'test_helper'

class QuizTest < ActiveSupport::TestCase

  def setup
    @quiz = quizzes(:one)
    @user = users(:michael)
  end
  
  test "other quiz is valid" do
    assert @quiz.valid?
  end
  
  test "user id should be present" do
    @quiz.user_id = nil
    assert_not @quiz.valid?
  end

 test "title should be present" do
    @quiz.title = nil
    assert_not @quiz.valid?
  end
  
  test "order should be most recect first" do
    assert_equal quizzes(:most_recent), Quiz.first
  end
  
  test "associated questions should be destroyed" do
    @quiz.save
    @quiz.questions.create!(title: "Something Creative")
    assert_difference 'Question.count', -2 do
      @quiz.destroy
    end
    
  end
end
