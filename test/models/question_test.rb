require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  def setup
    @q1 = questions(:one)
    @q2 = questions(:two)
    @q3 = questions(:three)
  end
  
  test "title should be present" do
    @q1.title = nil
    assert_not @q1.valid?
     @q2.title = nil
    assert_not @q2.valid?
     @q3.title = nil
    assert_not @q3.valid?
  end
  
end
