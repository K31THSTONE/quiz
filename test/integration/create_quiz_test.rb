require 'test_helper'

class CreateQuizTest < ActionDispatch::IntegrationTest

  test "can create a quiz" do
    get "/quizzes/new"
    assert_response :success
   
    post "/quizzes",
      params: { quiz: { title: "can create", category: "successful" } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end
  
  test "can create a question" do
    get "/questions/new"
    assert_response :success
   
    post "/questions",
      params: { question: { title: "can create"} }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end
  
  test "can create a answer" do
    get "/answers/new"
    assert_response :success
   
    post "/answers",
      params: { answer: { content: "can create", correct: 'true'} }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end
  
end
