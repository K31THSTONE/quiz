require 'test_helper'
 
class QuizzesInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test "quiz interface" do
    log_in_as(@user)
    get root_path
    #assert_select 'div.pagination'
    # Invalid submission
    assert_no_difference 'Quiz.count' do
      post quizzes_path, params: { quiz: { content: " " } }
    end
    #assert_select 'div#error_explanation'
    # Valid submission
    #@content = "This quiz really ties the room together"
    #assert_difference 'Quiz.count', 1 do
    #  post quizzes_path, params: { quiz: { content: "This quiz really ties the room together" } }
    #end
    assert_response :success
    #assert_match content, response.body
    # Delete post
    #assert_select 'a', text: 'delete'
    first_quiz = @user.quizzes.paginate(page: 1).first
    #assert_difference 'Quiz.count', -1 do
    #  delete quiz_path(first_quiz)
    #end
    # Visit different user (no delete links)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0
  end

end