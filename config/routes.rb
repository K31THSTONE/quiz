Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  root 'static_pages#home'
  get  '/help',           to: 'static_pages#help'
  get  '/about',          to: 'static_pages#about'
  get  '/contact',        to: 'static_pages#contact' 
  get  '/signup',         to: 'users#new'
  post '/signup',         to: 'users#create'
  get  '/login',          to: 'sessions#new'
  post '/login',          to: 'sessions#create'
  delete '/logout',       to: 'sessions#destroy'
  get  '/createQuiz',     to: 'quizzes#new'
  post '/createQuiz',     to: 'quizzes#create'
  get  '/listQuiz',       to: 'quizzes#list'
  get  '/createQuestion', to: 'questions#new'
  post '/createQuestion', to: 'questions#create'
  get  '/createAnswer',   to: 'answers#new'
  post '/createAnswer',   to: 'answers#create'
  get  '/takeQuiz',       to: 'quizzes#take'
  post '/takeQuiz',       to: 'quizzes#take'
  get  '/showQuiz',       to: 'quizzes#show'
  get  '/results',        to: 'scores#show'
  post '/results',        to: 'scores#show'
  get  '/contact-me',     to: 'messages#new', as: 'new_message'
  post '/contact-me',     to: 'messages#create', as: 'create_message'
  get  '/detailedResults',to: 'scores#showDetailed'
  post '/detailedResults',to: 'scores#showDetailed'

  
  resources :users do
    member do
      get :following, :followers
    end
  end
  
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :quizzes,             only: [:create, :destroy]
  resources :questions
  resources :answers
  resources :relationships,       only: [:create, :destroy]
  
  # Redirect to error page if invalid url
  get  '*path',           to: 'static_pages#error'
  
end
